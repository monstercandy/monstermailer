/*
instead of:

    // send the message and get a callback with an error or details of the message that was sent
  email.send({
     text:    "i hope this works", 
     from:    "you <username@your-email.com>", 
     to:      "someone <someone@your-email.com>, another <another@your-email.com>",
     cc:      "else <else@your-email.com>",
     subject: "testing emailjs"
  }, function(err, message) { console.log("xxxxxxxxxxxxxxxxx", err || message); });



  email.sendAsync({
     text:    "i hope this works", 
     from:    "you <username@your-email.com>", 
     to:      "someone <someone@your-email.com>, another <another@your-email.com>",
     cc:      "else <else@your-email.com>",
     subject: "testing emailjs"
  }).then((res)=>{
    console.log("res", res)
  }).catch((ex)=>{
    console.log("exception", ex)
  })
  
*/


var emailjs = require("emailjs");

if(!emailjs.server.connectOrig) {
   emailjs.server.connectOrig = emailjs.server.connect
   emailjs.server.connect = function(connectHash){
   	   var re = emailjs.server.connectOrig(connectHash)
   	   re.sendAsync = function(sendHash) {
   	   	  return  new Promise(function(resolve, reject){
   	   	  	  re.send(sendHash, function(err, res){
   	   	  	 	if(err) return reject(err)
   	   	  	 	resolve(res)
   	   	  	 })
   	   	  })
   	   }
   	   return re
   }
}

module.exports = emailjs
