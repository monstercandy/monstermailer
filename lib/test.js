require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var context = {
   "user":{"u_id":"16473","u_name":"Kondor Csaba","u_primary_email":"csaba@gitarkurzusok.hu","u_primary_tel":"+36-30-4599557","u_primary_tel_confirmed":0,"u_primary_email_confirmed":1,"u_language":"hu","u_callin":"","u_deleted":0,"u_grant_access_for":"0","u_sms_notification":1,"u_email_login_fail":1,"u_email_login_ok":1,"created_at":"2010-06-03 00:03:53","updated_at":"2016-09-19 20:58:48"},
   "data":[{e_timestamp: 1234}],"timezone":""
 };
//"a","b"
// {"l_id":"ba0245a0ac879e38","l_server":"s1","l_user_id":"16473","l_username":"csaba@gitarkurzusok.hu","l_timestamp":"2018-02-17T15:47:41.426Z","l_ip":"80.99.74.175","l_subsystem":"accountapi","l_event_type":"auth-credential","l_agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36","l_successful":1,"l_other":"{\"msg\":\" ACCOUNT_OWNER \"}","l_processed":0,"l_country":"HU"}

var templates = require("MonsterTemplates")({
     "locales": ["en","hu"]
})
templates.renderFileAsync("templates/generic/account/loginlog-successful.html.test", context, {subject: true, texthash: true, locale: "hu"})
.then(x=>{
  console.log(x);
});
