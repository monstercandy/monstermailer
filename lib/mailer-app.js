// permissions needed: ["EMIT_LOG","INFO_EMAIL"]
var re = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "mailer"


    const util = require('util');

    var config = require('MonsterConfig');
    var fs = require("fs");
    var path = require("path");

    var me = require('MonsterExpress');
    const MError = me.Error

      var APP_DEFAULTS = {
		 "i18n-2": {
			 "locales": ["en","hu"]
		  },
         "max_request_body_size": 1024*1024*20,  // 20 mbytes here :(, attachments might be huge
         "site": {
           base_url: "https://your.service.provider.should.customize.this.one/",
           short_name: "Short Name of your Service Provider",
           long_name: "Long Name of your Service Provider",
           style: {
              body: "background-image: url(\"{{site.base_url}}img/background.jpg\");background-repeat: no-repeat;margin:0px",
              logo: "background-image: url(\"{{site.base_url}}img/logo2.png\");height:60px;display:block;background-repeat: no-repeat;width: 100%;margin-bottom: 10px;",
              content: "padding: 10px;font-size: 14pt;background: #eef0eb none repeat scroll 0 0;background: rgba(238, 240, 235, 0.5);",
           }
         }
      }

    setupDefaultConfig(config)

    config.appRestPrefix = "/mailer"


    var app = me.Express(config, moduleOptions);

    app.emailjs = require("mailer-emailjs.js");

    // see: https://github.com/eleith/emailjs#emailserverconnectoptions
    app.email  = app.emailjs.server.connect(config.get("smtp"));

    app.templates = require("MonsterTemplates")(config.get("i18n-2"))


    app.vali = require("MonsterValidators").ValidateJs()


    var router = app.PromiseRouter(config.appRestPrefix)

    const MonsterInfoLib = require("MonsterInfo")
    app.MonsterInfoEmail = MonsterInfoLib.email({config: app.config, accountServerRouter: app.ServersRelayer})


    router.post("/invalidate", function(req, res, next) {
      app.templates.invalidateCache()
      req.result = "ok"
      return next()

    })

    router.post("/sendmail", function(req, res, next) {

      var j = req.body.json

      var data
      return app.vali.async(j, {
         "locale": {inclusion: config.get("i18n-2").locales },
         "subject": {isString: {trim: true}},
         "template": {presence: true, format: /^[a-z0-9_\-]+\/[a-z0-9_\-]+$/},
      })
      .then((aData)=>{
         data = aData
         if(!j.context) j.context={}
         j.context.site = config.get("site")

         if(j.toAdmin) {
            j.to = config.get("admin_recipient");
            return Promise.resolve()
         }
         if((!j.toAccount)&&(!j.localeAsUser)) return Promise.resolve()

         return app.MonsterInfoEmail.GetInfo(j.toAccount || j.localeAsUser)
           .then((x)=>{
              data.locale = x.account.u_language
              if(j.toAccount) {
                j.context.account = x.account
                j.to = x.email.map((e)=>{return e.e_email}).join(", ");
              }
           })

      })
      .then(()=>{

         var fn = data.template+".html"
         var prefix = "generic"
         if(fs.existsSync(path.join("templates","custom", fn)))
            prefix = "custom"
         var templateFn = path.join("templates",prefix,fn)

         j.context.locale = data.locale || "en"

         return app.templates.renderFileAsync(templateFn, j.context, {subject: true, texthash: true, locale: data.locale})
      })
      .then((res)=>{

          var subjectFromTemplate= false
          if((!data.subject)&&(res.subject)) {
             data.subject = res.subject
             subjectFromTemplate = true
          }

          if(!data.subject)
            throw new MError("SUBJECT_IS_MISSING")

          var subject = config.get("subject_prefix") || ""
          if((subject)&&(data.subject))
             subject += " - "

          subject += data.subject

          if((j.context) && (subject) && (!subjectFromTemplate))
             subject = app.templates.render(subject, { locals: j.context })


          var message = {
             text:    res.text,
             from:    j.from || config.get("from"),
             to:      j.to,
             cc:      j.cc,
             bcc:     getBcc(j),
             subject: subject,
             attachment:
             [
                 {data:res.html, alternative:true}
             ]
          };

          if(app.vali.isArray(j.attachments)) {
             for(var q of j.attachments){
                if(q.path) throw new MError("PATH_NOT_SUPPORTED")
                if(!q.data) throw new MError("ATTACHMENT_DATA_IS_MISSING")

                message.attachment.push(q)
             }
          }

          var params = {to: j.to, template: data.template};
          if(j.bcc) params.bcc = j.bcc;
          if(j.cc) params.cc = j.cc;
          app.InsertEvent(req, {
    		    e_user_id: (j.toAccount || {}).user_id,
    		    e_event_type: "email-send", e_other: params
    		  })

          // var message = app.emailjs.message(headers)
          return app.email.sendAsync(message)
            .catch(ex=>{
                console.error("Error while sending mail", message.from, message.to, message.subject, ex)
                throw new MError("SMTP_ERROR", null, ex);
            })

      })
      .then((res)=>{
         req.sendOk();
      })


    })


    return app



    function setupDefaultConfig(config){
      var base_url = (config.get("site") || {}).base_url || APP_DEFAULTS.site.base_url

      config.defaults(APP_DEFAULTS)

      var site = config.get("site")
      if(!site) return
      var style = site.style
      if(!style) return

      var changed = false
      for(var q of ["body","logo"]) {
         var o = style[q]
         style[q] = style[q].replace('{{site.base_url}}', base_url)
         if(o != style[q])
           changed = true
      }

      config.set("site", site)

    }

    function getBcc(j){
       var s = j.bcc || ""
       return s + (s ? ", " : "") + config.get("bcc")
    }

}

module.exports = re
