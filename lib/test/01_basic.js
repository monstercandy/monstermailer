require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../mailer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    describe('cache', function() {
        it('invalidate', function() {

             return assert.isFulfilled(

                 mapi.postAsync( "/invalidate", {})
                   .then((res)=>{
                      assert.equal("ok", res.result) 

                   })

             )

        })


    })

    describe('sendmail', function() {
        it('without parameters', function() {

             return assert.isFulfilled(

                 mapi.postAsync( "/sendmail", {})
                   .then((res)=>{
                      assert.fail("should not be here")

                   })
                   .catch((ex)=>{
                      // console.log(ex)
                      assert.equal("VALIDATION_ERROR",ex.message)
                   })

             )

        })

        it('dotdot in template name', function() {

             return assert.isFulfilled(

                 mapi.postAsync( "/sendmail", {"template": "../test"})
                   .then((res)=>{
                      assert.fail("should not be here")

                   })
                   .catch((ex)=>{
                      // console.log(ex)
                      assert.equal("VALIDATION_ERROR",ex.message)
                   })

             )

        })

        it('not existing template', function() {

             return assert.isFulfilled(

                 mapi.postAsync( "/sendmail", {"template": "test/not_existing", "subject": "foo!"})
                   .then((res)=>{
                      assert.fail("should not be here")

                   })
                   .catch((ex)=>{
                      assert.equal("INTERNAL_ERROR",ex.message)
                   })

             )

        })


        const emailParameters = {"template": "test/test", "to": "email1@email.cc, email2@email.cc", "subject": "foo!", "context": {"name": "Joe"}}
        const expectedParameters = { text: 'Hey overridden Joe this is just a simple test with a link pointing to the site\n[https://your.service.provider.should.customize.this.one/] .',
  from: 'from@test.cc',
  to: 'email1@email.cc, email2@email.cc',
  cc: undefined,
  bcc: 'bcc@test.cc',
  subject: 'foo!',
  attachment:
   [ { data: '<html>\n\t<head>\n\t\t<meta charset="utf-8">\n\t</head>\n\t<body>\n\t  Hey overridden Joe this is just a simple test with a link pointing to the <a href="https://your.service.provider.should.customize.this.one/">site</a>.\n\t</body>\n</html>\n',
       alternative: true } ] }


        it('email with site and other parameters', function(done) {

             app.email.send = function(sendHash, callback) {
                  // console.log(sendHash)
                  assert.deepEqual(sendHash, expectedParameters)

                  callback(null, "ok")
             }

             mapi.postAsync( "/sendmail", emailParameters)
               .then((res)=>{
                  assert.equal(res.result,"ok")
                  done()

               })
               .catch((ex)=>{
                  done(ex)                  
               })


        })


        it('toAccount should trigger a MonsterInfoEmail lookup', function(done) {

            var aEmailParameters = {"template": "test/test2", "subject": "foo!", "context": {"name": "Joe"}}
            delete aEmailParameters.to
            aEmailParameters.toAccount = {user_id: 12345, emailCategory: "TECH"}

            app.MonsterInfoEmail = {
              GetInfo: function(data){
                  assert.deepEqual(data, {user_id: 12345, emailCategory: "TECH"})
                  return Promise.resolve({email:[{e_email: "fooo@bar.hu"},{e_email: "some@foo.bar"}], account:{u_some_account_param: "xxx", u_language:"hu"}})
              }
            }

             app.email.send = function(sendHash, callback) {
                  //  console.log(sendHash)
                  assert.deepEqual(sendHash, { text: 'Hey xxx',
  from: 'from@test.cc',
  to: 'fooo@bar.hu, some@foo.bar',
  cc: undefined,
  bcc: 'bcc@test.cc',
  subject: 'foo!',
  attachment:
   [ { data: '<html>\n\t<head>\n\t\t<meta charset="utf-8">\n\t</head>\n\t<body>\n\t  Hey xxx\n\t</body>\n</html>\n',
       alternative: true } ] })

                  callback(null, "ok")
             }

             mapi.postAsync( "/sendmail", aEmailParameters)
               .then((res)=>{
                  assert.equal(res.result,"ok")
                  done()

               })
               .catch((ex)=>{
                  done(ex)                  
               })



        })


        const bccEmailParameters = {"template": "test/test", "to": "email1@email.cc, email2@email.cc", "bcc": "something@bcc.hu", "subject": "foo!", "context": {"name": "Joe"}}
        const bccExpectedParameters = { text: 'Hey overridden Joe this is just a simple test with a link pointing to the site\n[https://your.service.provider.should.customize.this.one/] .',
  from: 'from@test.cc',
  to: 'email1@email.cc, email2@email.cc',
  cc: undefined,
  bcc: 'something@bcc.hu, bcc@test.cc',
  subject: 'foo!',
  attachment:
   [ { data: '<html>\n\t<head>\n\t\t<meta charset="utf-8">\n\t</head>\n\t<body>\n\t  Hey overridden Joe this is just a simple test with a link pointing to the <a href="https://your.service.provider.should.customize.this.one/">site</a>.\n\t</body>\n</html>\n',
       alternative: true } ] }



        it('email with bcc (default one should be appended)', function(done) {

             app.email.send = function(sendHash, callback) {
                  // console.log(sendHash)
                  assert.deepEqual(sendHash, expectedParameters)

                  callback(null, "ok")
             }

             mapi.postAsync( "/sendmail", emailParameters)
               .then((res)=>{
                  assert.equal(res.result,"ok")
                  done()

               })
               .catch((ex)=>{
                  done(ex)                  
               })


        })

        it('same without explicit subject', function(done) {

             app.email.send = function(sendHash, callback) {

                  expectedParameters.subject = "custom subject"

                  assert.deepEqual(sendHash, expectedParameters)

                  callback(null, "ok")
             }

             delete emailParameters.subject
             mapi.postAsync( "/sendmail", emailParameters)
               .then((res)=>{
                  assert.equal(res.result,"ok")
                  done()

               })
               .catch((ex)=>{
                  done(ex)                  
               })


        })




        it('layout template example', function(done) {

             app.email.send = function(sendHash, callback) {
//Köszönjük, hogy megtisztel bizalmával, és minket választott weboldala\nkiszolgálásához!\nLétrehoztunk az Ön számára egy hozzáférést a Monster Media webhoszting\n
                  assert.propertyVal(sendHash, "text", "[https://your.service.provider.should.customize.this.one/] \nhttps://your.service.provider.should.customize.this.one/\n[https://your.service.provider.should.customize.this.one/]\n\nTovábbi szép napot!\nShort Name of your Service Provider\n[https://your.service.provider.should.customize.this.one/]\nShort Name of your Service Provider @ Facebook")
                  assert.propertyVal(sendHash, "subject", "Regisztráció")

                  callback(null, "ok")
             }

             mapi.postAsync( "/sendmail", {"locale": "hu", "template": "test/layout_test", "to": "test@test.cc", 
                 "context": { "uri": "/xxxxxxxxxxxxx", "user": {"u_name": "Joe", "u_primary_email": "test@test.xx"} }
              })
               .then((res)=>{
                  assert.equal(res.result,"ok")
                  done()

               })
               .catch((ex)=>{
                  done(ex)                  
               })


        })

    })

})

