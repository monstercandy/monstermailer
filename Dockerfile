FROM monstercommon

ADD lib /opt/MonsterMailer/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterMailer/lib/bin/mailer-install.sh && /opt/MonsterMailer/lib/bin/mailer-test.sh

ENTRYPOINT ["/opt/MonsterMailer/lib/bin/mailer-start.sh"]
